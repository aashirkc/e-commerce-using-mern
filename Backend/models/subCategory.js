const mongoose = require('mongoose')

const categorySchema =  new mongoose.Schema(
    {
         category: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "category",
            required: true
        },
        name:{
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        
    },
    
    {
		collection: 'subCategory',
		timestamps: true,
		toObject: {
			virtuals: true,
		},
		toJson: {
			virtuals: true,
		},
	}
)

module.exports = mongoose.model("subCategory", categorySchema)