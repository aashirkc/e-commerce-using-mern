const mongoose = require('mongoose')

const productSchema =  new mongoose.Schema(
    {
        name:{
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
         price:{
            type: Number,
            required: true,
        },
        size:{
            type: String,
        },
         sold:{
            type: Number,
            required: true,
            default: 0,
        },
        quantity:{
            type: Number,
            required: true,
        },
        brand:{
            type: String,
        },
        color:{
            type: String,
            required: true,
        },
        unit:{
            type: String,
        },
        model:{
            type: String,
            required: false
        },
        category: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "category",
            required: true
        },
        subCategory:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "subCategory",
            required: true
        },
        photo: {
            data: Buffer,
            contentType: String
        },
        description:{
            type: String,
            required: true
        },
        shipping: {
            type: Boolean,
            required: false
        }

       
    },
    {
		collection: 'products',
		timestamps: true,
		toObject: {
			virtuals: true,
		},
		toJson: {
			virtuals: true,
		},
	}
)

module.exports = mongoose.model("products", productSchema)
