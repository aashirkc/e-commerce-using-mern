const mongoose = require('mongoose')

const categorySchema =  new mongoose.Schema(
    {
        name:{
            type: String,
            trim: true,
            required: true,
            unique: true,
            maxlength: 32
        },
        
    },
    {
		collection: 'category',
		timestamps: true,
		toObject: {
			virtuals: true,
		},
		toJson: {
			virtuals: true,
		},
	}
)

module.exports = mongoose.model("category", categorySchema)