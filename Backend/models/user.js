const mongoose = require('mongoose')
const crypto = require('crypto')
const uuidv1 = require('uuid')

const userSchema =  new mongoose.Schema(
    {
        name:{
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        email: {
            type: String,
            trim: true,
            required: true,
            unique: true,
        },
        address: {
            type: String,
            trim: true,
            required: true,
           
        },
         age: {
            type: Number,
            trim: true,
            required: true,
            
        },
        contactNo: {
            type: String,
            trim: true,
            required: true,
        },
        hashed_Password: {
            type: String,
            required: true,
        
        },
        about: {
            type: String,
                trim: true,  
        },
        salt: {
            type: String,

        },
        role: {
            type: Number,
            default: 0
        },
        history: {
            type: Array,
            default: []
        },
    },
    {
		collection: 'user',
		timestamps: true,
		toObject: {
			virtuals: true,
		},
		toJson: {
			virtuals: true,
		},
	}
)

//virtual field

userSchema.virtual('password')
.set(function(password){
    this._password = password;
    this.salt = uuidv1.v1()
    this.hashed_Password = this.encryptPassword(password)
})
.get(function(){
    return this._password
})

//this method is used to encrypt password
userSchema.methods = {

    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_Password
    },
    

    encryptPassword: function(password){
        if(!password) return '';
        try{
            return crypto.createHmac('sha1', this.salt)
                        .update(password)
                        .digest('hex')
        }catch(ex){
            return '';
        }
    }
}




module.exports = mongoose.model("User", userSchema)
