const Category = require('../models/category')
const SubCategory = require('../models/subCategory')
const _ = require('lodash');

exports.categoryById = async(req,res,next,id) => {
     try{
        let data = await Category.findById(id);
        if(!data) throw new AppError("Category not found");
        req.category = data
        next();
    }catch(ex){
        res.status(400).json({
            "status": "failed",
            "message": "category not found"
        })
    }
}

exports.read = async(req, res) => {
    return res.json({
        status: "sucess",
        message: "Data fetched successfully",
        data:req.category
    })
}

exports.getAllCategory = async(req,res) => {
    let data = await Category.find();
    if(!data) throw new AppError("Category not found");
    return res.json({
        status: "sucess",
        message: "Data fetched successfully",
        data: data
    })
}

exports.getAllCategorySub = async(req,res) => {
    let resData = []
    let data = await Category.find();
    for(let i= 0; i<data.length; i++){
        let categoryId = data[i]._id;
        let subCategory = await SubCategory.find({category: categoryId})
        let returnValue = {
            categoryId: categoryId,
            name: data[i].name,
            subCategory: subCategory
        }
        resData.push(returnValue)
    }
    if(!data) throw new AppError("Category not found");
    return res.json({
        status: "sucess",
        message: "Data fetched successfully",
        data: resData
    })
}

exports.remove = async(req, res) => {
    let category = req.category
    let deletedCategory = await category.remove()
    res.json({
        status:"success",
        message: "category deleted sucssfully",
        data: deletedCategory
    })
}

exports.create = async (req, res) => {
        const category = new Category(req.body)
        const data = await category.save()
        res.json({
            status: 'sucess',
            message: 'Category created sucessfully',
            data: data
        })
}

exports.update = async (req, res) => {
    console.log("req.body",req.body)
        const category = req.category
        category.name = req.body.name
        const data = await category.save()
        res.json({
            status: 'sucess',
            message: 'Category created sucessfully',
            data: data
        })
}

