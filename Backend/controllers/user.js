const User = require('../models/user')
const jwt = require('jsonwebtoken')//to generate signed token
const AppError = require('../utils/error');
const { Order } = require('../models/order');


exports.userById = async(req, res, next, id) => {
      try{
        let data = await User.findById(id);
        if(!data) throw new AppError("user not found");
        req.profile = data
        next();
    }catch(ex){
        res.status(400).json({
            "status": "failed",
            "message": "User not found"
        })
    }
}

exports.signup = async (req,res) => {
    console.log('here',req.body)
    const user = await new User(req.body)
    console.log('sdfafdsfadsfasdf',req.body)
    let data = await user.save()
    res.json({
        status:'sucess',
        message: 'User added Sucessfully',
        data:data
    })
}

exports.signin = async (req,res) => {
    //find the user based on email
    const { email, password } = req.body
    let user = await User.findOne({email: email})
    console.log(user)
    if(!user) throw new AppError("User not Found!! Please signup first.")

   //matching user given password

    if(!user.authenticate(password)){
        throw new AppError ("Email and password doesnt match!",401)
    }
    //generate a signed token with user id and secret
    const token =  jwt.sign({_id: user._id, role: user.role}, process.env.JWT_SECRET)

    //persist the token as 't' in cookie with expiry date
    res.cookie('t',"Bearer "+ token, {expire: new Date() + 9999})
    const {id,  name, role} = user
    res.json({
        status: "success",
        message: "User loggedin sucessfully",
        data:{id,email,name, role},
        token: "Bearer " + token
    })
}

exports.signout = async (req,res) => {
    res.clearCookie('t'),
   res.json({
        status:"success",
        message:"User signout sucessfully"
    })
}

exports.read = async(req, res) => {
    req.profile.hashed_password = undefined
     req.profile.salt = undefined
     res.json({
        status:"success",
        message:"User data retrieved sucessfully",
        data: req.profile
    })
}

exports.update = async(req, res) => {
   let data = await User.findOneAndUpdate(
       {_id: req.profile._id}, 
       {$set: req.body}, 
       {new: true})
   if(!data) throw new AppError("You are not authorized")
   data.hashed_password = undefined;
   data.salt = undefined
   res.json({
        status:"success",
        message:"User updated sucessfully",
        data: data
    })
}


exports.addOrderToUserHistory = async(req,res,next) => {
    let history = [];
    req.body.order.products.forEach((item) => {
        history.push({
            _id: item._id,
            name: item.name,
            description: item.description,
            category: item.category,
            quantity: item.count,
            transaction_id: req.body.order.transaction_id,
            amount: req.body.order.amount
        })
    })

      let data =   await User.findOneAndUpdate({_id: req.profile._id},{$push:{history:history}},{new:true})
      if(!data) throw new AppError("couldnot update user history")
      next()
}

exports.purchaseHistory = (req,res) => {
    Order.find({user: req.profile._id})
    .populate('user',"_id name")
    .sort('-created')
    .exec((err,order)=>{
        if(err){
              res.status(400).json({
                status:"success",
                error:err,
            })
        }
         res.json({
                status:"success",
                data:order,
            })

    })
}
