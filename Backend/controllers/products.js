const Product = require('../models/products');
const Category = require('../models/category');
const SubCategory = require('../models/subCategory')
const formidable = require('formidable')
const { catchAsync } = require('../helpers/error')
const _ = require('lodash');
const fs = require('fs');
const AppError = require('../utils/error');
const { query } = require('winston');
const category = require('../models/category');
const { max } = require('lodash');


exports.productById = async (req, res, next , id) => {
    try{
        let data = await Product.findById(id).populate('category') ;
        if(!data) throw new AppError("Product not found");
        req.product = data
        next();
    }catch(ex){
        res.status(400).json({
            "status": "failed",
            "message": "product not found"
        })
    }
}

exports.read = async(req, res) => {
    req.product.photo =  undefined
    return res.json({
        status: "sucess",
        message: "Data fetched successfully",
        data:req.product
    })
}

exports.remove = async(req, res) => {
    let product = req.product
    let deletedProduct = await product.remove()
    deletedProduct.photo = undefined
    res.json({
        status:"success",
        message: "product deleted sucssfully",
        data: deletedProduct
    })
}



exports.create =  async (req,res) => {
    let form = formidable({ multiples: true })
    form.parse(req,catchAsync(async (err, fields, files) => {
        if(err) {
            return res.status(400).json({
                status: "failed",
                err:err,
                message: "image cannot be uploaded"
            })
        }
        console.log("",fields,files)
        let products = await new Product(fields)

        if(files.photo) {
         
            if(files.photo.size > 5000000){
               return res.status(400).json({
                   status: 400,
                   message: "Image size must be less than 5mb"
               })
            }

        
            products.photo.data = fs.readFileSync(files.photo.path)
            products.photo.contentType = files.photo.type
        }

        let result = await products.save()
            result.photo = undefined
            res.json({
            status: "success",
            message: "Products added Successfully,",
            data: result

        })
        
        
    }))
}


exports.update =  async (req,res) => {
    let form = formidable({ multiples: true })      
    form.parse(req,async (err, fields, files) => {
        if(err) {
           return res.status(400).json({
                status: "failed",
                message: "image cannot be uploaded"
            })
        }
        console.log("fields",fields,files)
        let products = req.product
        products = _.extend(products, fields)

        if(files.photo) {
            console.log("sdfsdf",files.photo.size)
            if(files.photo.size > 5000000){
                 return res.status(400).json({
                   status: 400,
                   message: "Image size must be less than 5mb"
               })
            }
            products.photo.data = fs.readFileSync(files.photo.path)
            products.photo.contentType = files.photo.type
        }

        let result = await products.save()
            result.photo = undefined
            res.json({
            status: "success",
            message: "Products updated Successfully,",
            data: result

        })
        
        
    })
}


/* 
* sell / Arrival
* by sell = /products/list?sortBy=sold&order=desc&limit=4
* by arrival = /products/list?sortBy=createdAt&order=desc&limit=4
* if no params is sent, then all products are arrived
 */


exports.list =  async (req, res) => {
    let order = req.query.order ? req.query.order : 'asc'
    let sortBy = req.query.sortBy ? req.query.sortBy : '_id'
    let limit = req.query.limit ? parseInt(req.query.limit) : 6

     let data = await Product.find()
        .select("-photo")
        .populate('category')
        .sort([[sortBy, order]])
        .limit(limit)
        .exec()
    if(!data) throw new AppError("Data not Found", 400)

    res.json({
        status: "success",
        message: "Data Fetched Sucessfully",
        data: data
    })
}




/*
* It will find product based on the req product category
*/

exports.relatedProducts = async (req, res) => {
    let limit = req.query.limit ? parseInt(req.query.limit) : 6
    let data = await Product.find({_id: {$ne: req.product}, category: req.product.category})
                            .select('-photo')
                            .limit(limit)
                            .populate('category','_id name')
    if(!data) throw new AppError("Data not Found");

    res.json({
        status: "success",
        message: "Data Fetched Sucessfully",
        data: data
    })
}

exports.listCategories = async(req, res) => {
    console.log("Sdf")
    let data = await Product.find().distinct("category")
     if(!data) throw new AppError("Data not Found");
       res.json({
        status: "success",
        message: "Data Fetched Sucessfully",
        data: data
    })
}

exports.listBySubCategory = async(req,res) => {
    let subcategoryId = req.params.id 
    let data = await Product.find({subCategory: subcategoryId}).select('-photo').sort({price: 1})
     if(!data) throw new AppError("Data not Found");
       res.json({
        status: "success",
        message: "Data Fetched Sucessfully",
        data: data,
       
    })
}

exports.listByFilteredSubCategory = async(req,res) => {
    let data;
    let sort = req.query.sort;
    let maxPrice = req.query.maxPrice;
    let minPrice = req.query.minPrice;
    console.log(sort,maxPrice,minPrice)
    if(!sort) sort = 1
    if(maxPrice){
        data = await Product.find({
            subCategory:req.params.id, 
            price: {$gte: minPrice},
            price:{ $lte: maxPrice, }
        }).sort({price: -1})
        
    }

       res.json({
        status: "success",
        message: "Data Fetched Sucessfully",
        data: data,
       
    })
   
}


exports.search = async(req, res) => {

    let query = {}

    if(req.query.search){
        query.name = {$regex: req.query.search, $options:'i'}
        if(req.query.category){
            query.category = req.query.category
        }
    }
    let data = await Product.find(query)
                            .select("-photo")
     if(!data) throw new AppError("Data not Found");
       res.json({
        status: "success",
        message: "Data Fetched Sucessfully",
        data: data
    })
}


/**
 * 
 *returns the subcategory and products from category name
 *  
 */

 exports.productsByCategory = async(req,res) => {
     console.log("SDfsfd")
    let categoryName = req.params.categoryName;
    let categoryId = await Category.findOne({name: categoryName})
    let products = await Product.find({category:categoryId._id}).select('-photo')
    res.json({
        status: "success",
        message: "Data Fetched Sucessfully",
        data: products
    })
}


/**
 * list products by search
 * we will implement product search in react frontend
 * we will show categories in checkbox and price range in radio buttons
 * as the user clicks on those checkbox and radio buttons
 * we will make api request and show the products to users based on what he wants
 */



exports.listBySearch = async (req, res) => {
    let order = req.body.order ? req.body.order : "desc";
    let sortBy = req.body.sortBy ? req.body.sortBy : "_id";
    let limit = req.body.limit ? parseInt(req.body.limit) : 100;
    let skip = parseInt(req.body.skip);
    let findArgs = {};

    // console.log(order, sortBy, limit, skip, req.body.filters);
    // console.log("findArgs", findArgs);

    for (let key in req.body.filters) {
        if (req.body.filters[key].length > 0) {
            if (key === "price") {
                // gte -  greater than price [0-10]
                // lte - less than
                findArgs[key] = {
                    $gte: req.body.filters[key][0],
                    $lte: req.body.filters[key][1]
                };
            } else {
                findArgs[key] = req.body.filters[key];
            }
        }
    }



    Product.find(findArgs)
        .select("-photo")
        .populate("category")
        .sort([[sortBy, order]])
        .skip(skip)
        .limit(limit)
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: "Products not found"
                });
            }
            res.json({
                length: data.length,
                data: data
            });
        });
};


exports.listBySearchSubcategory = async (req, res) => {
    let order = req.body.order ? req.body.order : "desc";
    let sortBy = req.body.sortBy ? req.body.sortBy : "_id";
    let limit = req.body.limit ? parseInt(req.body.limit) : 100;
    let skip = parseInt(req.body.skip);
    let findArgs = {};

    // console.log(order, sortBy, limit, skip, req.body.filters);
    // console.log("findArgs", findArgs);

    for (let key in req.body.filters) {
        if (req.body.filters[key].length > 0) {
            if (key === "price") {
                // gte -  greater than price [0-10]
                // lte - less than
                findArgs[key] = {
                    $gte: req.body.filters[key][0],
                    $lte: req.body.filters[key][1]
                };
            } else {
                findArgs[key] = req.body.filters[key];
            }
        }
    }



    Product.find(findArgs)
        .select("-photo")
        .populate("category")
        .sort([[sortBy, order]])
        .skip(skip)
        .limit(limit)
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: "Products not found"
                });
            }
            res.json({
                length: data.length,
                data: data
            });
        });
};

exports.photo = async(req, res, next) => {
    if(req.product.photo.data) {
        res.set('Content-Type', req.product.photo.contentType)
        return res.send(req.product.photo.data);
    }
    next()
}

exports.decreaseQuantity = async(req,res,next) => {
    let bulkOps = req.body.order.products.map(item => {
        return {
            updateOne: {
                filter:{_id: item._id},
                update: {$inc: {quantity: -item.count, sold: +item.count                                                }}
            }
        }
    })

    Product.bulkWrite(bulkOps, {}, (error, products) => {
        if(error){
            return res.status(400).json({
                error: 'Could not update product'
            })
        }
    })
    next()
}
