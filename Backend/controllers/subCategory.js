const SubCategory = require('../models/subCategory')
const Category = require('../models/category')
const _ = require('lodash');

exports.subCategoryById = async(req,res,next,id) => {
     try{
        let data = await SubCategory.findById(id);
        if(!data) throw new AppError("subCategory not found");
        req.subCategory = data
        next();
    }catch(ex){
        res.status(400).json({
            "status": "failed",
            "message": "subCategory not found"
        })
    }
}

exports.findSubCategory = async(req,res) => {

    let categoryName = req.query.name
    let categoryId = await Category.findOne({name: categoryName})
    let subCategory = await SubCategory.find({category: categoryId._id})
    res.status(200).json({
            "status": "sucess",
            "data": subCategory
        })
}

exports.read = async(req, res) => {
    let data = await SubCategory.find({category: req.query.id})
    return res.json({
        status: "sucess",
        message: "Data fetched successfully",
        data:data
    })
}

exports.getAllCategory = async(req,res) => {
    let data = await SubCategory.find();
    if(!data) throw new AppError("subCategory not found");
    return res.json({
        status: "sucess",
        message: "Data fetched successfully",
        data: data
    })
}

exports.remove = async(req, res) => {
    let subCategory = req.subCategory
    let deletedCategory = await subCategory.remove()
    res.json({
        status:"success",
        message: "subCategory deleted sucssfully",
        data: deletedCategory
    })
}

exports.create = async (req, res) => {
        const subCategory = new SubCategory(req.body)
        const data = await subCategory.save()
        res.json({
            status: 'sucess',
            message: 'subCategory created sucessfully',
            data: data
        })
}

exports.update = async (req, res) => {
        const subCategory = req.subCategory
        subCategory.name = req.body.name
        const data = await subCategory.save()
        res.json({
            status: 'sucess',
            message: 'subCategory created sucessfully',
            data: data
        })
}

