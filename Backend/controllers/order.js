const {Order, CartItem} = require('../models/order');
const AppError = require('../utils/error');


exports.orderById = async(req, res, next, id) => {
    try{
          console.log(req.body)
        let data = await Order.findById(id)
        req.order = data
        next();
    }catch(ex){
        res.status(400).json({
            "status": "failed",
            "message": ex
        })
    }
}
exports.create = async(req,res) => {
    console.log('create order:', req.body)

    req.body.order.user = req.profile
    const order = new Order(req.body.order)
    let data = await order.save()
    if(!data) throw new AppError("cannot save order", 400)
    return res.json({
        status: "sucess",
        message: "Data fetched successfully",
        data:data
    })
};

exports.listOrder = async(req,res) => {
            Order.find()
            .populate('user', "_id name address")
            .sort('_created')
            .exec((err,orders)=>{
                if(err){
                    return res.json({
                        status: "error",
                        message: "couldnot find error",
                        data:data
                    })
                }
                return res.json({
                        status: "success",
                        data:orders
            })
        })
}

exports.getStatusValue = (req,res) => {
    res.json(Order.schema.path('status').enumValues)
}


exports.updateOrderStatus =  async(req,res) => {
    let data = await Order.findOneAndUpdate(
        {_id: req.body.orderId},
         {status: req.body.status}, 
         {new: true},)
        res.json(data)
}
