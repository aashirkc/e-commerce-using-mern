const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');//authorization check   
const User = require('../models/user')
const { catchAsync } = require('../helpers/error');
const AppError = require('../utils/error');

exports.auth = catchAsync(async (req, res, next) => {
  console.log('hey')
  let token = req.header("Authorization");
	
  if (!token)
    return res.status(401).send({ 
      status: "failed",
      message: "Access denied. No token provided." 
    });

  try {
    token = token.startsWith('Bearer') ? token : `Bearer ${token}`;
    token = token.slice(7) 
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    console.log('here')
    let userCheck = await User.findOne({_id: decoded._id});
    req.user = userCheck;
    console.log(req.user)
    if(!userCheck) throw new AppError("Invalid user");
    next();
  } catch (ex) {
    res.status(400).send({ 
      status: "failed",
      message: "Invalid token." 
    });
  }
});

exports.requiesignin = expressJwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'auth',
    algorithms: ['RS256']
});

exports.isAdmin =  catchAsync(async (req,res, next) => {
  if(req.user.role === 1){
    next()
  }else{
    throw new AppError("Acess denied! unauthorized try!")
  }
}); 