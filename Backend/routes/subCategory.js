const express = require('express')
const router = express.Router();
const { catchAsync } = require('../helpers/error')
const { auth, requiesignin , isAdmin} = require('../middleware/auth')
const{ create, subCategoryById ,read, getAllsubCategory,remove,findSubCategory, update} = require('../controllers/subCategory') 


router.post('/add', auth,isAdmin, catchAsync(create));

router.get('/read', catchAsync(read))

router.delete("/remove/:subCategoryId", catchAsync(remove));

router.put("/update/:subCategoryId", catchAsync(update));

router.get("/find/by/category",catchAsync(findSubCategory));

router.param("subCategoryId", subCategoryById)



module.exports = router;