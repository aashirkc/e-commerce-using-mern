const express = require('express')
const router = express.Router();
const { catchAsync } = require('../helpers/error')
const { auth, requiesignin , isAdmin} = require('../middleware/auth')
const{ create, categoryById ,read, getAllCategory,remove,getAllCategorySub, update} = require('../controllers/category') 


router.post('/add', auth,isAdmin, catchAsync(create));

router.get('/read/:categoryId', catchAsync(read))

router.delete("/remove/:categoryId", catchAsync(remove));

router.put("/update/:categoryId", catchAsync(update));

router.get("/getall", catchAsync(getAllCategory))

router.get("/getall/subcategory", catchAsync(getAllCategorySub))

router.param("categoryId", categoryById)



module.exports = router;