const express = require('express')
const router = express.Router();
const { catchAsync } = require('../helpers/error')
const { auth,requiesignin, isAdmin } = require('../middleware/auth')
const{ signup, signin, signout, userById, read, update, purchaseHistory } = require('../controllers/user') 

router.post('/signup', catchAsync(signup));

router.post('/signin', catchAsync(signin));

router.get('/signout',  catchAsync(signout));

router.get('/read/:userId', catchAsync(read))

router.get('/orders/by/:userId', purchaseHistory)

router.put('/update/:userId', catchAsync(update))

router.param("userId", userById)

module.exports = router;