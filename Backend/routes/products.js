const express = require('express')
const router = express.Router();
const { catchAsync } = require('../helpers/error')
const { auth, requiesignin , isAdmin} = require('../middleware/auth')
const{ 
    create, 
    productById, 
    read, 
    remove, 
    update, 
    list, 
    relatedProducts, 
    listBySearch, 
    listCategories,
    photo,
    listBySubCategory,
    listBySearchSubcategory,
    listByFilteredSubCategory,
    search,
    productsByCategory
 } = require('../controllers/products') 

router.get('/read/:productId', catchAsync(read));

router.post('/add', auth,isAdmin, catchAsync(create));

router.delete("/remove/:productId", catchAsync(remove));

router.put("/update/:productId", catchAsync(update));

router.get("/related/:productId", catchAsync(relatedProducts))

router.get('/categories', catchAsync(listCategories))

router.get('/list', catchAsync(list));

router.get('/find', catchAsync(search))

router.post("/by/search", listBySearch);

router.post("/by/search/subcategory", listBySearchSubcategory);

router.get("/by/subcategory/:id", catchAsync(listBySubCategory));

router.get("/filter/by/subcategory/:id", catchAsync(listByFilteredSubCategory));

router.get("/photo/:productId", photo)

router.param("productId",   productById);

// get product by category name and return subcategory and products related to that category

router.get("/category/:categoryName", productsByCategory)



module.exports = router;