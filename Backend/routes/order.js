
const router = require('express').Router();
const { catchAsync } = require('../helpers/error')
const { auth,isAdmin } = require('../middleware/auth')
const{ create,
     listOrder,
      getStatusValue,
     updateOrderStatus, 
     orderById } = require('../controllers/order') 
const{ userById, addOrderToUserHistory } = require('../controllers/user') 
const{ decreaseQuantity } = require('../controllers/products') 

router.post('/create/:userId', addOrderToUserHistory, decreaseQuantity, catchAsync(create))

router.get('/list/:userId',auth, isAdmin, catchAsync(listOrder))

router.get('/list/status/:userId',auth, isAdmin, catchAsync(getStatusValue))

router.put('/update/status',auth, isAdmin, catchAsync(updateOrderStatus))

router.param("userId", userById)
router.param("orderId", orderById)

module.exports = router;