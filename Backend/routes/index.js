const router = require('express').Router();
const categoryRouter = require('./category')
const subCategoryRouter = require('./subCategory')
const productRouter = require('./products')
const userRouter = require('./user');
const orderRouter = require('./order');

router.use('/api/user', userRouter);
router.use('/api/category', categoryRouter);
router.use('/api/subcategory', subCategoryRouter);
router.use('/api/products', productRouter)
router.use('/api/order', orderRouter)

module.exports = router;