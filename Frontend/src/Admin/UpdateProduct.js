import React, {useState, useEffect} from 'react';
import { isAuthenticated } from '../Auth';
import {Link, Redirect} from 'react-router-dom';
import Layout from '../core/Layout';
import {getSingleProducts, getCategory, updateProduct} from './apiAdmin' 


const UpdateProduct = ({match}) => {
    const [ values, setValues] = useState({
        name:'',
        description:'',
        price:'',
        category: '',
        categories: [],
        shipping: '',
        quantity:'',
        size:'',
        sold:'',
        photo:'',
        unit:'',
        loading: false,
        error: '',
        success: false,
        createProduct: '',
        redirectToProfile: false,
        formData: ''
    })
    

    const  { 
        name,
        description,
        price,
        category,
        categories,
        shipping,
        quantity,
        size,
        unit,
        sold,
        loading,
        error,
        success,
        createdProduct,
        redirectToProfile,
        formData} = values

    //destructure user and token from localstorage

    const {data, token } = isAuthenticated()


    const init = (productId) => {
        getSingleProducts(productId).then(data=>{
            if(data && data.status && data.status === error){
                setValues({...values, error:data.message})
            }else{
               
                setValues({   name: data.data.name,
                                        description: data.data.description,
                                        price: data.data.price,
                                        category: data.data.category._id,
                                        shipping: data.data.shipping,
                                        quantity: data.data.quantity,
                                        size: data.data.size,
                                        unit: data.data.unit,
                                        formData: new FormData()
                                       })
               
            }
        })
    }

    //load category and set form data
    const initCategories = async() => {
        await getCategory().then(data=>{
            if(!data){
                 setValues({...values,error: "cannot find categories"})
            }
            if(data  && data.status === 'error'){
                setValues({...values,error:data.message})
            } else {
                setValues({...values,
                    categories: data.data, 
                    formData: new FormData()
                })
            }
        })
    }

    useEffect(() => {
        init(match.params.productId);
        initCategories()
    }, [])


    const handleChange = name => event => {
        const value =
         name === 'photo' ? event.target.files[0] : event.target.value;
        setValues({...values, [name]: value})
    }

    const clickSubmit = (e) => {
        e.preventDefault();
        setValues({...values  ,formData:new FormData(), error: '', loading: false})
        console.log(values,"Sdfsdf")

        //make request to api create category
       for ( var key in values ) {
            formData.append(key, values[key]);
        }
        
        updateProduct(match.params.productId, token, formData)
        .then(data=>{
            if(data.status === "error" ){
               setValues({...values,error: data.message, success: false})
            }else if(data.status === "success"){
                setValues({
                    ...values,
                        name:'',
                        description:'',
                        price:'',
                        category: '',
                        shipping: '',
                        quantity:'',
                        size:'',
                        sold:'',
                        photo:'',
                        loading: false,
                        success: true,
                        error: '',
                        createProduct: data.name,
                        redirectToProfile: true,
                })
            }
        })
    }

      const showError = () => (
        <div className="alert alert-danger" style={{display: error ? '': 'none'}}>
            {error}
        </div>
    )

     const showSuccess = () => (
        <div className="alert alert-info" style={{display: success ? '': 'none'}}>
             product is updated
        </div>
     )

     const redirectUser = () => {
         if(redirectToProfile){
             if(!error) {
                 return <Redirect to="/admin/products"/>
             }
         }
     }


    const newPostForm = () => (
        <form className="mb-3" onSubmit={clickSubmit}>
            <h4>
                Post Photo
            </h4>
            <div className="form-group">
                <label className="btn btn-secondary">
                     <input type="file" 
                    name="photo"
                    onChange={handleChange('photo')}
                    accept="image/*"
                    />
                </label>
               
            </div>
            <div className="form-group">
                <label className="text-muted">Name</label>
                <input type="text" 
                    className="form-control" 
                    value={name} 
                    onChange={handleChange('name')}
                   
                    />
            </div>
            <div className="form-group">
                <label className="text-muted">Description</label>
                <textarea  
                    className="form-control" 
                    value={description} 
                    onChange={handleChange('description')}
                  />
            </div>
             <div className="form-group">
                <label className="text-muted">Price</label>
                <input type="number" 
                    className="form-control" 
                    value={price} 
                    onChange={handleChange('price')}
                   
                    />
            </div>
            <div className="form-group">
                <label className="text-muted">Size</label>
                <input type="text" 
                    className="form-control" 
                    value={size} 
                    onChange={handleChange('size')}
                   
                    />
            </div>
             <div className="form-group">
                <label className="text-muted">Unit</label>
                <input type="text" 
                    className="form-control" 
                    value={unit} 
                    onChange={handleChange('unit')}
                   
                    />
            </div>
             <div className="form-group">
                <label className="text-muted">Quantity</label>
                <input type="number" 
                    className="form-control" 
                    value={quantity} 
                    onChange={handleChange('quantity')}
                   
                    />
            </div>
            <div className="form-group">
                <label className="text-muted">Category</label>
                <select
                    className="form-control" 
                    value={category}
                    onChange={handleChange('category')}
                    >
                        <option >Please Select</option>
                            {categories && categories.map((c,i) => (
                                <option key={i} value = {c._id}>{c.name}</option>
                            ))}
                </select>
            </div>
            <div className="form-group">
                <label className="text-muted">Shipping</label>
                <select
                    className="form-control"
                    value={shipping} 
                    onChange={handleChange('shipping')}
                    >
                        <option >Please Select</option>
                        <option value={false}>No</option>
                        <option value={true}>yes</option>
                        
                </select>
            </div>
            <button className="btn btn-outline-primary">Update Product</button>
        </form>
    )



    const goBack = () => (
        <div className = "mt-5">
            <Link to="/admin/dashboard" className="text-warning">Back to dashboard</Link>
        </div>
    )



    return (
        <Layout title="Add a new Product" 
                description={`Good Day ${data.name}`}
                className = "container-fluid"
                >
          <div  className="row" >
              <div className="col-md-8 offset-md-2">
                {showError()}
                {showSuccess()}
                {newPostForm()}
                {goBack()}
                {redirectUser()}
              </div>
             
          </div>
           
        </Layout>
    )
}

export default UpdateProduct;