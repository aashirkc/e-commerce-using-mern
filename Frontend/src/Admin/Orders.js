import React, {useState, useEffect} from 'react';
import { isAuthenticated } from '../Auth';
import {Link} from 'react-router-dom';
import Layout from '../core/Layout';
import {listOrders, getStatusVlaues, updateOrderStatus} from './apiAdmin' ;
import moment from 'moment';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';


const Orders = () => {
    const [orders, setOrders] = useState([])
    const [status, setStaus] = useState([])

    const {data, token} = isAuthenticated()

    useEffect(() => {
       loadOrders();
       loadStatusValues();
    }, [])

    const loadOrders = () => {  
        listOrders(data.id, token)
        .then(res => {
            if(res && res.status && res.status === 'error'){
                console.log(res)
            }else{
                setOrders(res.data)
            }
        })
    }

     const loadStatusValues = () => {  
        getStatusVlaues(data.id, token)
        .then(res => {
            if(res && res.status && res.status === 'error'){
                console.log(res)
            }else{
                setStaus(res)
            }
        })
    }

    

    const showStaus = (o) => {
        return(
            <div className="form-group">
                <span>{o.status}</span>
                <select className="form-control" onChange={e => handleStatusChange(e,o._id)}>
                    <option >update Staus</option>
                    {status.map((status, index)=> (
                        <option key={index} value={status}>{status}</option>
                    ))}
                </select>
            </div>
        )
    }

    const handleStatusChange = (e,orderId) => {
        updateOrderStatus(data.id, token, orderId, e.target.value)
        .then(res=>{
            if(res && res.status && res.status === 'error'){
                console.log("status update failed")
            }else{
                 loadOrders()
            }
        })
    }

    const showOrderLength = (orders) => {
       if(orders && orders.length > 0){
           return (
               <h1 className="text-danger ">Total length: {orders.length}</h1>
           )
       }else {
           return <h1 className="text-danger">No ordres</h1>
       }
    }

    return (
        <Layout title="Orders" 
                description={`Good Day ${data.name}`}
                className = "container-fluid"
                >
          <div  className="row" >
              <div className="col-12">
                {showOrderLength(orders)}
                 <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">user iD</th>
                        <th scope="col">Transaction id</th>
                        <th>Order By</th>
                        <th>Ordered on</th>
                        <th>Delivery Address</th>
                        <th>Status</th>
                        <th>Product Name</th>
                        <th>Total Producs</th>
                    </tr>
                    </thead>
                    <tbody>
                        {orders && orders.map((o,i)=>{
                          return(  <tr key={i}>
                                        <td>{o._id}</td>
                                        <td>{o.transaction_id}</td>
                                        <td>{o.user.name}</td>
                                        <td>{moment(o.createdAt).fromNow()}</td>
                                        <td>{o.address}</td>
                                        <td>{showStaus(o)}</td>
                                        <td>{o.products.map((p,i)=>(
                                                    <ul>
                                                        <li>
                                                            Name:{p.name}
                                                        </li>
                                                        <li>
                                                            Price:Rs{p.price}
                                                        </li>
                                                        <li>
                                                            Total:{p.count}
                                                        </li>
                                                        <li>
                                                            Id:{p._id}
                                                        </li>
                                                   </ul>
                                               
                                         ))}
                                            </td>
                                        <td>{o.products.length}</td>
                                    </tr>
                          )  
                        })}
                       
                    </tbody>
                </table>
                
              </div>
             
          </div>
           
        </Layout>
    )

}

export default Orders;