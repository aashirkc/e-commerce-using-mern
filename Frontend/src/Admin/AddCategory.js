import React, {useState, useEffect} from 'react';
import { isAuthenticated } from '../Auth';
import {Link} from 'react-router-dom';
import Layout from '../core/Layout';
import {createCategory, getCategory,getSubCategory, updateCategory,createSubCategory} from './apiAdmin' 


const AddCategory = () => {
    const [ name, setName] = useState('')
    const [editState, setEditState] = useState(false)
    const [ error, setError] = useState(false)
    const [ success, setSuccess] = useState(false)
    const [categories, setCategory] = useState([])
    const [subCategories, setSubCategories] = useState([])
    const [categoryId, setCategoryId] = useState('')
    const [subCategory, setSubCategory] = useState('')


    const init = async() => {
        await getCategory().then(data=>{
            if(!data){
                 setValues({...values,error: "cannot find categories"})
            }
            if(data  && data.status === 'error'){
            } else {
                setCategory(data.data)
            }
        })
    }

    useEffect(() => {
       init()
    }, [])

    //destructure user and token from localstorage

    const {data, token } = isAuthenticated()

    const handleChange = (e) => {
        setError(''),
        setName(e.target.value)
    }


    const editCategory = (e) => {
        e.preventDefault();
        updateCategory(categoryId, name)
        .then(data => {
            if(data.status === "error" ){
                 setError(true)
                 setSuccess(false)
            }else{
                setError(false);
                setCategoryId('')
                setName('');
                setEditState(false)
                init()
            }
        })
    }


    const clickSubmit =  (e) => {
        e.preventDefault();
        setError('');
        setSuccess(false);
        //make request to api create category
        createCategory(token, {name})
        .then(data=>{
            if(data.status === "error" || "fail"){
                 setError(true)
                 setSuccess(false)
            }else{
                setError(false);
                setSuccess(true);
                init()
            }
        })
    }

    const cancelEdit = () => {
        setEditState(false)
        setName('')
    }

    const addSubCategory = (e) => {
        e.preventDefault()
        let data = {
            category: categoryId,
            name: subCategory
        }
        
         createSubCategory(token, data)
        .then(data=>{
             if(data.status === "error"){
                 setError(true)
                 setSuccess(false)
            }else{
                setError(false);
                setSuccess(true);
                setSubCategory('');
                findSubCategory(categoryId)
                init()
            }
        })
    }

   
    

    const newCategoryForm = () => (
        <form >
            <div className="form-group">
                <label className="text-muted">Name</label>
                <input type="text" 
                    className="form-control" 
                    value={name} 
                    onChange={handleChange}
                    autoFocus
                    required/>
            </div>

            {
               editState === false &&
               <button onClick={clickSubmit} className="btn btn-outline-primary">Create Category</button>
            }

             {
               editState === true &&
               <button className="btn btn-outline-primary" onClick={editCategory}>Edit Category</button>
            }

             {
               editState === true &&
               <button className="btn btn-outline-primary ml-3" onClick={cancelEdit}>Cancel</button>
            }
            
        </form>
    )

   const findSubCategory = (id) => {
       console.log('here',id)
        getSubCategory(id).
        then(data => {
            if(data.status === "error"){
                 setError(true)
                 setSuccess(false)
            }else{
                setSubCategories(data.data);
            }
        })
    }

  

    const handleClick = (c) => {
        setEditState(true)
        setName(c.name)
        setError(false)
        setCategoryId(c._id)
        findSubCategory(c._id)
    }

    const handleSubCategory = (e) => {
        setSubCategory(e.target.value)
    }

    const newSubCategoryForm = () => (
           <form >
            <div className="form-group">
                <label className="text-muted">Name</label>
                <input type="text" 
                    className="form-control" 
                    value={subCategory} 
                    onChange={handleSubCategory}
                    autoFocus
                    required/>
            </div>

            
             
               <button onClick={addSubCategory} className="btn btn-outline-primary">Create sub-Category</button>
            

             

        </form>
    )

      const showSuccess = () =>{
        if(success) {
            return <h3 className="text-success">{name} is created</h3>
        }
    }

    const showError = () =>{
        if(error) {
            return <h3 className="text-danger">{name} should be unique</h3>
        }
    }

    const goBack = () => (
        <div className = "mt-5">
            <Link to="/admin/dashboard" className="text-warning">Back to dashboard</Link>
        </div>
    )

    const sidebar = () =>(
        <div className="col-md-2 " >
                    <div className="card">
                        <div className="card-header name">
                            Categories
                        </div>
                        <div className="card-body">
                            <ul style={{listStyle: "none"}}>
                                {
                                    categories && categories.map((c,i) => {
                                        return (
                                           <div key={i}> 
                                           <li  onClick={() => handleClick(c) }>{c.name}</li> 
                                           <hr/>
                                           </div>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                    </div>  
                     <div className="card">
                        <div className="card-header name">
                            SubCategories
                        </div>
                        <div className="card-body">
                            <ul style={{listStyle: "none"}}>
                                {
                                    subCategories && subCategories.map((c,i) => {
                                        return (
                                           <div key={i}> 
                                           <li  >{c.name}</li> 
                                           <hr/>
                                           </div>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                    </div>             
                </div>
    )




    return (
        <Layout title="Add a new Category" 
                description={`Good Day ${data.name}`}
                className = "container-fluid"
                >
          <div  className="row" >

               {sidebar()}
              
              <div className="col-md-4 ">
                {showSuccess()}
                {showError()}
                {newCategoryForm()}
                {goBack()}
              </div>

              <div className="col-md-4 ">
               
                {newSubCategoryForm()}
               
              </div>
             
             
          </div>
           
        </Layout>
    )
}

export default AddCategory;