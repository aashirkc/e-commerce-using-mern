import React, {useEffect, useState} from 'react';
import { isAuthenticated } from '../Auth';
import {Link} from 'react-router-dom';
import Layout from '../core/Layout';
import {getProducts, deleteProduct} from './apiAdmin'
import moment from 'moment'


const ManageProducts = () => {

    const [products, setProducts] = useState([])

    const loadProducts = () => {
        getProducts().then(data=>{
            if(data && data.status && data.status === "error"){
                console.log(data.message)
            }else{
                setProducts(data.data)
            }
        })
    }

    const{data,token} = isAuthenticated()

    const destroy = productId => {
        deleteProduct(productId, token).then(data=>{
             if(data && data.status && data.status === "error"){
                console.log(data.message)
            }else{
               loadProducts()
            }
        })
    }

    useEffect(()=>{
        loadProducts()
    }, [])


    return (
        <Layout title="Manage Products" description="Shopping sathi">
          
            <h2 className="mb-4">
                Manage Products
            </h2>

            <div className="row">
                <div className="col-12">
                      <table className="table">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">price</th>
                        <th>category</th>
                        <th>size</th>
                        <th>quantity</th>
                        <th>unit</th>
                        <th>createdAt</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        {products && products.map((p,i)=>(
                            <tr key={i}>
                                <td>
                                    {p.name}
                                </td>
                                 <td>
                                    {p.price}
                                </td>
                                 <td>
                                    {p.category.name}
                                </td>
                                 <td>
                                    {p.size}
                                </td>
                                 <td>
                                    {p.quantity}
                                </td>
                                 <td>
                                    {p.unit}
                                </td>
                                 <td>
                                    {moment(p.createdAt).fromNow()}
                                </td>
                                <td>
                                    <Link to={`/update/product/${p._id}`}>
                                        <button className="btn btn-primary mr-3">
                                            update
                                        </button>
                                    </Link>
                                     <button onClick={() => destroy(p._id)} className="btn btn-danger">
                                        delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                    </table>
                </div>
            </div>
           
            

        </Layout>
    )
}

export default ManageProducts
