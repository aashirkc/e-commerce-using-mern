import { API } from '../config'
 
 
export  const createCategory = (token, category) => {
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/category/add`, {
            method: "POST",
            headers: {
                "Authorization": `${token}`,
                "Content-Type": "application/json"
            },

            body: JSON.stringify(category)
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

    export  const createSubCategory = (token, subCategory) => {
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/subcategory/add`, {
            method: "POST",
            headers: {
                "Authorization": `${token}`,
                "Content-Type": "application/json"
            },

            body: JSON.stringify(subCategory)
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }


    export  const createProduct = (token, product) => {
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/products/add`, {
            method: "POST",
            headers: {
                "Authorization": `${token}`,
                "Accept": 'application/json'
            },

            body: product
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }


      export  const getCategory = (token, product) => {
      
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/category/getall`, {
            method: "GET",
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }


 export  const getSubCategory = (categoryId) => {
      
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/subcategory/read?id=${categoryId}`, {
            method: "GET",
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }


      export  const listOrders = (userId,token) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/order/list/${userId}`, {
            method: "GET",
             headers: {
                "Authorization": `${token}`,
                "Accept": 'application/json'
            },
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

      export  const getStatusVlaues = (userId,token) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/order/list/status/${userId}`, {
            method: "GET",
             headers: {
                "Authorization": `${token}`,
                "Accept": 'application/json'
            },
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

    export  const updateOrderStatus = (userId, token, orderId, status) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/order/update/status`, {
            method: "PUT",
             headers: {
                "Authorization": `${token}`,
                 'Content-Type': 'application/json',
                "accept": 'application/json'
            },
            body: JSON.stringify({status, orderId})
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }


// perfrom crud on products


export  const getProducts = () => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/products/list`, {
            method: "GET",
             
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

export  const getSingleProducts = (productId) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/products/read/${productId}`, {
            method: "GET",
             
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }


 export  const deleteProduct  = (productId, token) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/products/remove/${productId}`, {
            method: "DELETE",
             headers: {
                "Authorization": `${token}`,
                 'Content-Type': 'application/json',
                "accept": 'application/json'
            },
           
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

export  const updateProduct  = (productId, token, product) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/products/update/${productId}`, {
            method: "PUT",
             headers: {
                "Authorization": `${token}`,
                 "Accept": 'application/json'
            },
            body: product
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

export const updateCategory = (categoryId, name) => {

     return fetch(`${API}/category/update/${categoryId}`, {
            method: "PUT",
             headers: {
               'Content-Type': 'application/json',
                 "Accept": 'application/json'
            },
            body: JSON.stringify({name:name})
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
}