 
import { API } from '../config'
 
 
  const signup = (user) => {
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/user/signup`, {
            method: "POST",
            headers: {
               
                "Content-Type": "application/json"
            },

            body: JSON.stringify(user)
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

     const signin = (user) => {
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/user/signin`, {
            method: "POST",
            headers: {
               
                "Content-Type": "application/json"
            },

            body: JSON.stringify(user)
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

    const authenticate = (data, next) => {
        if(typeof window!== "undefined") {
            localStorage.setItem("jwt", JSON.stringify(data));
            next();
        }
    }

    const signout = (next) => {
          if(typeof window!== "undefined") {
            localStorage.removeItem("jwt");
            next();
            return fetch(`${API}/user/signout`,{
                method: "GET"
            })
            .then(response => {
                console.log("signout", response)
            })
            .catch(err => console.log(err))
        }
    }

    const isAuthenticated = () => {
        if(typeof window == 'undefined'){
            return false;
        }
        if(localStorage.getItem('jwt')){
            return JSON.parse(localStorage.getItem('jwt'))
        } else {
            return false
        }
    }

    export {
        authenticate,
        isAuthenticated,
        signup,
        signin,
        signout
    };