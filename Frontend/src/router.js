import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Signup from './user/Signup';
import Signin from './user/Signin';
import Home from './core/Home';
import PrivateRoute from './Auth/PrivateRoute'
import Dashboard from './user/userDashboard';
import AdminRoute from './Auth/AdminRoute';
import AdminDashboard from './user/AdminDashboard';
import AddCategory from './Admin/AddCategory';
import AddProduct from './Admin/AddProducts';
import Shop from './core/Shop';
import Product from './core/Products';
import Cart from './core/Cart';
import Orders from './Admin/Orders';
import Profile from './user/Profile'
import ManageProducts from './Admin/ManageProduct';
import UpdateProduct from './Admin/UpdateProduct';
import ShopSubCategory from './core/shopSubcategory';
import ShopCategory from './core/ShopCategory';



const Routes = () => {
    return(
        <BrowserRouter>
         
            <Switch>
                <Route path = "/" exact component={Home} />
                <Route path = "/shop" exact component={Shop} />
                <Route path = "/product/:productId" exact component={Product} />
                <Route path = "/signin" exact component={Signin} />
                <Route path = "/signup" exact component={Signup} />
                 <Route path = "/cart" exact component={Cart} />
                <PrivateRoute path = "/user/dashboard" exact component={Dashboard} />
                 <PrivateRoute path = "/profile/:userId" exact component={Profile} />
                <AdminRoute path = "/admin/dashboard" exact component={AdminDashboard} />
                <AdminRoute path = "/admin/products" exact component={ManageProducts} /> 
                <AdminRoute path = "/create/category" exact component={AddCategory} />
                <AdminRoute path = "/create/product" exact component={AddProduct} />
                <AdminRoute path = "/update/product/:productId" exact component={UpdateProduct} />
                <AdminRoute path = "/admin/orders" exact component={Orders} />
                <AdminRoute path = "/subcategory/:subCategoryId" exact component={ShopSubCategory} />
                 <AdminRoute path = "/category/:categoryId" exact component={ShopCategory} />
            </Switch>
        </BrowserRouter>
    )
}

export default Routes;
