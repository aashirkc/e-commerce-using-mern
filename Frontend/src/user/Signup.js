import React,{useState} from 'react'
import {Link} from 'react-router-dom'
import Layout from '../core/Layout'
import {signup} from '../Auth'

const Signup = () => {

    

    const [values, setValues] = useState({
        name: '',
        email: '',
        password: '',
        address: '',
        age: '',
        contactNo: '',
        error: '',
        success: false
    })

    const {name, email, password, address, age, contactNo, success, error} = values 

    const handleChange = name => event => {
        setValues({...values, error:false, [name]: event.target.value})
    }

  

    const clickSubmit = (event) => {
        event.preventDefault()
        signup({name,email,password, address, age, contactNo})
        .then(data=>{
            console.log(data)
            if(data.status === 'error'){
                setValues({...values, error: data.message, success:false})
            }else{
                setValues({
                    ...values,
                    name: '',
                    email: '',
                    password: '',
                    address: '',
                    age: '',
                    contactNo: '',
                    error:'',
                    success: true
                })
            }
        })
    }

    const signupForm = () => (
        <form>
            <div className="form-group">
                    <label className="text-muted">Name</label>
                    <input onChange={handleChange('name')} 
                    value={name} 
                    type="text" 
                    className="form-control" /> 
            </div>
             <div className="form-group">
                    <label className="text-muted">Email</label>
                    <input 
                    onChange={handleChange('email')} 
                    type="email"
                     value={email}  
                    className="form-control" /> 
            </div>
             <div className="form-group">
                    <label className="text-muted">Password</label>
                    <input 
                    onChange={handleChange('password')} 
                    type="password" 
                     value={password} 
                    className="form-control" /> 
            </div>
             <div className="form-group">
                    <label className="text-muted">Address</label>
                    <input 
                    onChange={handleChange('address')} 
                    type="text" 
                     value={address} 
                    className="form-control" /> 
            </div>
            <div className="form-group">
                    <label className="text-muted">Age</label>
                    <input 
                    onChange={handleChange('age')} 
                    type="number" 
                     value={age} 
                    className="form-control" /> 
            </div>
             <div className="form-group">
                    <label className="text-muted">Contact No</label>
                    <input 
                    onChange={handleChange('contactNo')} 
                    type="number" 
                     value={contactNo} 
                    className="form-control" /> 
            </div>
            <button onClick={clickSubmit} className="btn btn-primary">
                Submit
            </button>
        </form>
    )

    const showError = () => (
        <div className="alert alert-danger" style={{display: error ? '': 'none'}}>
            {error}
        </div>
    )

     const showSuccess = () => (
        <div className="alert alert-info" style={{display: success ? '': 'none'}}>
            New account is created.. proceed to <Link to="/signin">sign in</Link>
        </div>
     )

    return (
          <Layout title="Signup Page" 
          description="Sign up to Shopping sathi"
          className="container col-md-8 offset-md-2">
              {showSuccess()}
              {showError()}
            {signupForm()}
          </Layout>
    )
}


export default Signup;