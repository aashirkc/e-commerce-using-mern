import React, { useEffect, useState } from 'react';
import { isAuthenticated } from '../Auth';
import {Link} from 'react-router-dom';
import Layout from '../core/Layout';
import {getPurchaseHistory} from './apiUser'
import moment from 'moment'


const Dashboard = () => {

    const[history, setHistory] = useState([])

    const {data: {id,email,name, role}} = isAuthenticated();

    const {token} = isAuthenticated()

    const init = (userId, token) => {
        getPurchaseHistory(userId, token).then(data=>{
            if(data && data.status && data.status === 'error'){
                console.log(data)
            }else{
                setHistory(data.data)
            }
        })
    }

    useEffect(()=>{
        init(id, token )
    } ,[])


    const userLinks = () => {
        return (
            <div className="card">
                <h4 className="card-header">
                    User Links
                 </h4>
                <ul className="list-group">
                    <li className="list-group-item">
                        <Link className="nav-link" to="/cart" >My cart</Link>
                    </li>
                    <li className="list-group-item">
                        <Link className="nav-link" to={`/profile/${id}`} > Profile Update</Link>
                    </li>
                 
                </ul>
            </div>
        )
    }


    const userInfo = () => {
        return (
              <div className="card mb-5">
                <h3 className="card-header">User Information</h3>
                <ul className="list-group">
                    <li className="list-group-item">Name = {name}</li>
                    <li className="list-group-item">Email = {email}</li>
                    <li className="list-group-item">Role = {role === 1 ? "Admin" : "Registered User"}</li>
                </ul>
            </div>
        )
    }

    const purchaseHistory = (history) => {
        return (
             <div className="card mb-5">
                <h3 className="card-header">Purchase History</h3>
                <ul className="list-group">
                    <li className="list-group-item">
                        {history.map((h,i)=>(
                            <div>
                                {h.products.map((p,i) => (
                                    <div key={i}>
                                        <span>Product Name : {p.name}</span> <br/>
                                        <span>Product Price : Rs{p.price}</span>  <br/>
                                        <span>Date : {moment(p.createdAt).fromNow()}</span> <br/>
                                        <hr/>
                                    </div>
                                    
                                ))}
                            </div>
                        ))}
                    </li>
                  
                </ul>
            </div>
        )
    }

    return(
        <Layout title="Dashboard" 
                description={`Good Day ${name}`}
                className="container-fluid">
          <div  className="row" >
              <div className="col-3">
                {userLinks()}
              </div>
              <div className="col-9">
                {userInfo()}
                {purchaseHistory(history)}
              </div>

          </div>
           
        </Layout>
    )
}

export default Dashboard;