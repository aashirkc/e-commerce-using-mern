import React, {useState, useEffect} from 'react';
import { isAuthenticated } from '../Auth';
import {Link, Redirect} from 'react-router-dom';
import Layout from '../core/Layout';
import {read, update, updateUser} from './apiUser'

const Profile = (props) => {
 
    const [values, setValues] =useState({
        name:'',
        password: '',
        address: '',
        age: '',
        contactNo: '',
        error:'',
        success: false
    })

    const {token} = isAuthenticated()

    const {
        name,
        password,
        address,
        age,
        contactNo,
        error,
        success
    } = values

    const init = (userId) => {
       read(userId, token).then(data=>{
           if(data && data.status && data.status === "error"){
               setValues({...values, error: true})
           }else{
               setValues({...values, name:data.data.name, 
                                    address:data.data.address, 
                                    age: data.data.age, 
                                    contactNo:data.data.contactNo})
           }
       })
    }

    useEffect(()=>{
        init(props.match.params.userId)
    }, [])

   const handleChange = name => e => {
        setValues({...values , error: false, [name]:e.target.value})
    }


    const clickSubmit = e => {
        e.preventDefault();
        update(props.match.params.userId, token, {name, password, address, age, contactNo})
        .then(data => {
            if(data && data.status && data.status === "error"){
               console.log(data.error)
           } else {
                updateUser(data.data, () => {
                    setValues({ ...values,   name:data.data.name, 
                                            address:data.data.address, 
                                            age: data.data.age, 
                                            contactNo:data.data.contactNo,
                                            success: true})
                })
           }
        })
    }

    const redirectUser = (success) => {
        if(success === true){
            return <Redirect to="/user/dashboard"/>
        }
    }

    const profileUpdate = (name, password, address, age, contactNo) => (
        <form>
            <div className="form-group">
                <label className="text-muted">
                    Name
                </label>
                <input type="text" 
                        onChange={handleChange('name')}
                        className="form-control"
                        value={name}
                         />
            </div>
             
             <div className="form-group">
                <label className="text-muted">
                    Password
                </label>
                <input type="text" 
                        onChange={handleChange('password')}
                        className="form-control"
                        value={password}
                         />
            </div>
             <div className="form-group">
                <label className="text-muted">
                    Address
                </label>
                <input type="text" 
                        onChange={handleChange('address')}
                        className="form-control"
                        value={address}
                         />
            </div>
             <div className="form-group">
                <label className="text-muted">
                    age
                </label>
                <input type="number" 
                        onChange={handleChange('age')}
                        className="form-control"
                        value={age}
                         />
            </div>
             <div className="form-group">
                <label className="text-muted">
                    Contact no
                </label>
                <input type="number" 
                        onChange={handleChange('contactNo')}
                        className="form-control"
                        value={contactNo}
                         />
            </div>
            <button className="btn btn-primary ml-2 mb-4" onClick={clickSubmit}>
                Submit
            </button>
        </form>
    )
    



   return (
        <Layout title="Profile" description="Shopping sathi">
            
            <h2 className="mb-4">
               Profile Update
            </h2>
            {profileUpdate(name, password, address, age, contactNo)}
            {redirectUser(success)}
          
            

        </Layout>
    )

}

export default Profile