import { API } from '../config'


export  const read = (userId, token) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/user/read/${userId}`, {
                        method: "GET",
                         headers: {
                                "Authorization": `${token}`
                            },
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }


 export  const update = (userId, token, user) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/user/update/${userId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `${token}`
            },

            body: JSON.stringify(user)
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

export const updateUser = (user, next) => {
    if(typeof window !== undefined){
        if(localStorage.getItem('jwt')){
            let auth = JSON.parse(localStorage.getItem('jwt'))
            auth.data = user;
            localStorage.setItem('jwt',JSON.stringify(auth))
            next()
        }
    }
}

export  const getPurchaseHistory = (userId, token) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/user/orders/by/${userId}`, {
                        method: "GET",
                         headers: {
                                "Authorization": `${token}`
                            },
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }