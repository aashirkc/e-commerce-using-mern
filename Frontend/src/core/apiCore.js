import { API } from '../config'
import queryString from 'query-string'
 
export  const   getproducts = (sortBy) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/products/list?sortBy=${sortBy}&order=desc&limit=6`, {
                        method: "GET",
                    })
                    .then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
                 }
 
 export  const   list = params => {
                    let query = queryString.stringify(params) 
                    console.log(query)
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/products/find?${query}`, {
                        method: "GET",
                    })
                    .then(response => {
                    return response.json()
                    })
                    .catch( err => {
                    console.log(err)
                    })
                 }              
               

export      const   getCategory = (token, product) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/category/getall`, {
                        method: "GET",
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }

export      const   getCategorySub = (token, product) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/category/getall/subcategory`, {
                        method: "GET",
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }


export      const   getProductsFromSubCategory = (id) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/products/by/subcategory/${id}`, {
                        method: "GET",
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }

export  const getFilteredProducts = (skip, limit, filters = {}) => {
    const data = {
        limit, skip, filters
    }
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/products/by/search`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },

            body: JSON.stringify(data)
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

export  const getFilteredProductsFromSubcategory = (skip, limit, filters = {}) => {
    const data = {
        limit, skip, filters
    }
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/products/by/search/subcategory`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },

            body: JSON.stringify(data)
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

    

export  const getProductsFromCategory = (name) => {
        return fetch(`${API}/products/category/${name}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },

        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

export const getSubcategoryFromCategory = (name) => {
      return fetch(`${API}/subcategory/find/by/category?name=${name}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },

        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }


export  const  read = (productId) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/products/read/${productId}`, {
                        method: "GET",
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }

export      const   listRelated = (productId) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/products/related/${productId}`, {
                        method: "GET",
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }

export  const createOrder = (userId, token, createOrderData) => {
   
        // console.log(name, email, password, address, age, contactNo )
        return fetch(`${API}/order/create/${userId}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `${token}`
            },

            body: JSON.stringify({order: createOrderData})
        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }

export const getSubCategory = (name) => {
                    // console.log(name, email, password, address, age, contactNo )
                    return fetch(`${API}/subcategory/find/by/category?name=${name}`, {
                        method: "GET",
                    })
                    .then(response => {
                        return response.json()
                    })
                    .catch( err => {
                        console.log(err)
                    })
                }


  
export const getFilterFromSubcategory = (id, sort, min, max) => {
      return fetch(`${API}/products/filter/by/subcategory/${id}?sort=${sort}&maxPrice=${max}&minPrice=${min}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },

        })
        .then(response => {
            return response.json()
        })
        .catch( err => {
            console.log(err)
        })
    }