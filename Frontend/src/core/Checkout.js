import React, {useState, useEffect} from 'react'
import { isAuthenticated } from '../Auth';
import { Link } from 'react-router-dom';
import { set } from 'lodash';
import { createOrder } from './apiCore';
import { emptyCart } from './CartHelper';



const Checkout = ({products, setRun = f => f , run = undefined}) =>{

    const [address , setAddress] = useState('')
    const [success , setSuccess] = useState(false)

    const getTotal = () => {
        return products.reduce((currentValue, nextValue)=> {
                return currentValue + nextValue.count * nextValue.price 
        }, 0)
    } 

     const {data, token} = isAuthenticated()

    const handleAddress = event => {
        setAddress(event.target.value)
    }

    const submitRequest = (products) => {

        const createOrderData = {
            products: products, 
            transaction_id : 1,
            amount: getTotal(),
            address: address
        }
        let userId = data.id
        createOrder(userId, token, createOrderData)
        .then(data=>{
            if(data.status && data.sataus === 'error'){
                console.log(data)
            }else{
                emptyCart(()=>{
                    setRun(!run);
                    setSuccess(true)
                    console.log('payment Success and empty cart')
                })
            }

            
        })

         
    }

    const sucessBlock =  () => {
        return (
            success && <div className = "alert alert-info">
                Your order have been processed, it may take 2 to 3 days
            </div>
        )
    }

    const showCheckout = () => {
        return isAuthenticated()? (
                    <div>
                        <label className="text-muted">Delivery Address</label>
                        <textarea   onChange={handleAddress}
                                    className="form-control mb-3"
                                    value={address}>
                            
                        </textarea>
                        {products.length>0 && 
                        <button onClick={() => submitRequest(products)} className="btn btn-success">
                            Checkout
                         </button>}
                        
                    </div>
                    
                ):(
                     <Link to="/signin">
                        <button className="btn btn-primary">
                            Sign in to checkout
                        </button>
                    </Link>
                )
        }

    return (
            <div>
                {sucessBlock()}
                <h2>Total: Rs{getTotal()}</h2>
                
                {showCheckout()}
            </div>
    ) 
   
}


export default Checkout;