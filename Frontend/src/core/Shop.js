import React, {useState, useEffect} from 'react'
import Layout from './Layout'
import Card from './Card'
import {getCategory, getFilteredProducts} from './apiCore'
import Checkbox from './CheckBox'
import Radiobox from './Radiobox'
import {prices} from './FixedPrice'


const Shop = () => {

    const [myFilters, setMyFilters] = useState({
        filters:{category: [], price: []}
    })

    const [categories, setCategories] = useState([])
    const [error, setError] = useState(false)
    const [limit, setLimit] = useState(6)
    const [skip, setSkip] = useState(0)
    const [length, setLength] = useState(0)
    const [filteredResults, setFilteredResults] = useState('')


      //load category and set form data
    const init = async() => {
        await getCategory().then(data=>{
            if(!data){
                setError("Please Reload")
                return
            }
            if(data  && data.status === 'error'){
                setError(data.message)
            } else {
                setCategories(data.data)
            }
        })

    }

    useEffect(() => {
        init();
    },[])


    const handleFilter = (filters, filterBy) => {
        // console.log("shop",filters, filterBy)
        const newFilters = {...myFilters}
        newFilters.filters[filterBy] = filters
        if(filterBy === "price"){
            let priceValues = handlePrice(filters)
            newFilters.filters[filterBy] = priceValues  
        }

        loadFilterResults(myFilters.filters)
        setMyFilters(newFilters)
    }

    
    const handlePrice = value => {
        const data = prices
        let array = []
        for(let key in data) {
            if(data[key]._id === parseInt(value)){
                array = data[key].array
            }
        }
        return array
    }


    const loadFilterResults = (newFilters) => {
        getFilteredProducts(skip, limit, newFilters)
        .then(data => {
            if(data.status && data.status === "error"){
                setError(err.message)
            } else {
                console.log("sdfsdf", data)
                setFilteredResults(data.data)
                setLength(data.data.length)
                setSkip(0)
            }
        })
    }

    const loadMore = () => {
        let toSkip = skip + limit
        getFilteredProducts(toSkip, limit, myFilters.filters)
        .then(data => {
            if(data.status && data.status === "error"){
                setError(err.message)
            } else {
                console.log("sdfsdf", data)
                setFilteredResults([...filteredResults, ...data.data])
                setLength(data.data.length)
                setSkip(0)
            }
        })
    }

    const loadMoreButton = () => {
        return (
            length > 0  && (
                <button onClick={loadMore} className="btn btn-warning mb-5">
                    Load More
                </button>
            )
        )
    }
 
    return (
        <Layout title="Shop Page" description="Shopping sathi">
            <div className="row">
                <div className="col-4">
                    <h4>categories</h4>
                    <ul>
                          <Checkbox categories={categories}
                                    handleFilters= {filters=>
                                        handleFilter(filters, "category")
                                    }/>
                    </ul>

                     <h4> Price Range</h4>
                     <div>
                        <Radiobox prices={prices}
                                    handleFilters= {filters=>
                                        handleFilter(filters, "price")
                                    }/>
                     </div>
                   
                
                </div>
                 <div className="col-8">
                        <h2 className="mb-4">
                                    Products
                        </h2>
                        <div className="row">
                                {filteredResults && filteredResults.map((p,i)=>(
                                    
                                       <div key={i} className="col-4 mb-3">
                                             <Card  product={p} />
                                       </div>
                                    
                                ))}
                        </div>
                        <hr/>
                        {loadMoreButton()}
                </div>
            </div>

            
            

        </Layout>
    )
}

export default Shop;