import React, {useState, useEffect} from 'react'
import Layout from './Layout'
import {getCategory, list} from './apiCore'
import Card from './Card'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const Search = () => {
    const [data, setData] = useState({
        categories: [],
        category: '',
        search: '',
        results: [],
        searched: false
    })
    const {categories,category,search,results,searched} = data


    const loadCategory = () => {
        getCategory().then(data=>{
            if(data && data.status && data.status === 'error'){
                console.log(data)
            }else{
                if(data) setData({...data, categories: data.data})
            }
        })
    }

    useEffect(() => {
       loadCategory()
    }, [])


    const searchData = () => {
        if(search){
              list({search: search || undefined, category:category}) 
              .then(res=>{
                  if(res.status && res.status === 'error'){
                      console.log(res.message)
                  }else{
                      setData({...data, results: res, searched:true})
                  }
              })
        }
     
    }

    const searchSubmit = (e) =>{
        e.preventDefault()
        searchData()
    }

    const handleChange = (name) => event =>{
        setData({...data, [name]:event.target.value, searched: false})
    }


    const searchedProducts = (results = []) => {
        
       return( <div className="row">
                    {results.data && results.data.map((p,i) => (
                        <Card key={i} product={p} />
                    ))}
             </div>
       )
    }

    const searchForm = () => (
        <form onSubmit={searchSubmit}>
          <span style={{border: "none",backgroundColor: "white",height: "37px"}} className="input-group-text">
              <div className="input-group input-group-lg">
                  <div style={{border: 'none', height:"29px"}} className="input-group-prepend">
                        <select style={{border: 'none',backgroundColor: "#ff9900", fontSize: "11px", height:"29px"}} className="btn " onChange={handleChange("category")}>
                            <option value="All">Category</option>
                            {categories && categories.map((c,i)=> 
                                    (<option key={i} value={c._id}>{c.name}</option>) 
                                    )}
                        </select>
                  </div>
                    <input style={{height: "29px",fontSize: "20px"}}  type="search" 
                    className="form-control" 
                    onChange={handleChange('search')} 
                    placeholder="Search"/>
                     <button style={{border: 'none', backgroundColor: "#ff9900"}}  ><FontAwesomeIcon icon={faSearch} /></button>
              </div>
               
                 
            
          </span>
        </form>
    )


    return (
        <div className="row">
            <div className="container ">
                {searchForm()}

            </div>
           
        </div>
    )
};

export default Search;