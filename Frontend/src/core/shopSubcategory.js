import React, {useState, useEffect} from 'react'
import Layout from './Layout'
import Card from './Card'
import {getCategory, getFilteredProducts, getFilterFromSubcategory, getProductsFromSubCategory} from './apiCore'
import InputRange from 'react-input-range';
import Checkbox from './CheckBox'
import Radiobox from './Radiobox'
import {prices} from './FixedPrice'
import 'react-input-range/lib/css/index.css';


const ShopSubCategory = (props) => {

    const [myFilters, setMyFilters] = useState({
        filters:{category: [], price: []}
    })

    const [categories, setCategories] = useState([])
    const [error, setError] = useState(false)
    const [limit, setLimit] = useState(6)
    const [skip, setSkip] = useState(0)
    const [subCategoryID, setSubCategoryId] = useState('')
    const [length, setLength] = useState(0)
    const [priceRange, setPriceRange] = useState(Number)
    const [maxValue, setMaxValue] = useState(2000);
    const [minValue, setMinValue] = useState(0)
    const [filteredResults, setFilteredResults] = useState('')


      //load category and set form data
    const init = async() => {
        await getCategory().then(data=>{
            if(!data){
                setError("Please Reload")
                return
            }
            if(data  && data.status === 'error'){
                setError(data.message)
            } else {
                setCategories(data.data)
            }
        })

    }

    useEffect(() => {
        const subCategoryId = props.match.params.subCategoryId
        setSubCategoryId(props.match.params.subCategoryId)
        ProductsFromSubCategory(subCategoryId) 
        init();
    },[props])

    const ProductsFromSubCategory = async (id) => {
       await getProductsFromSubCategory(id).then(data=> {
            if(data && data.status === "error"){
                console.log(data)

            }else {
                
                setFilteredResults(data.data)
                setMaxValue(data.data.slice(-1)[0].price) 
                setPriceRange(data.data.slice(-1)[0].price - 500)
                setMinValue(data.data[0].price)

            }
        })

    }


    const handleFilter = (filters, filterBy) => {
        const newFilters = {...myFilters}
        newFilters.filters[filterBy] = filters
        if(filterBy === "price"){
            let priceValues = handlePrice(filters)
            newFilters.filters[filterBy] = priceValues  
        }

        loadFilterResults(myFilters.filters)
        setMyFilters(newFilters)
    }

    
    const handlePrice = value => {
        const data = prices
        let array = []
        for(let key in data) {
            if(data[key]._id === parseInt(value)){
                array = data[key].array
            }
        }
        return array
    }


    // const loadFilterResults = (newFilters) => {
    //     getFilteredProducts(skip, limit, newFilters)
    //     .then(data => {
    //         if(data.status && data.status === "error"){
    //             setError(err.message)
    //         } else {
    //             setFilteredResults(data.data)
    //             setLength(data.data.length)
    //             setSkip(0)
    //         }
    //     })
    // }

    const loadMore = () => {
        let toSkip = skip + limit
        getFilteredProducts(toSkip, limit, myFilters.filters)
        .then(data => {
            if(data.status && data.status === "error"){
                setError(err.message)
            } else {
                setFilteredResults([...filteredResults, ...data.data])
                setLength(data.data.length)
                setSkip(0)
            }
        })
    }

    const loadMoreButton = () => {
        return (
            length > 0  && (
                <button onClick={loadMore} className="btn btn-warning mb-5">
                    Load More
                </button>
            )
        )
    }
 
    const changeValue = (value) => {
        setPriceRange(value)
    }

    const completeChange = (value) => {
        getFilterFromSubcategory(subCategoryID, 1, minValue, priceRange).then(
            data=> {
                setFilteredResults(data.data)
            }
        )
    }

    return (
        <Layout title="Shop Page" description="Shopping sathi">
            <div className="row">
                <div className="col-3 categories">
                    <h4 className="ml-3">categories</h4>
                    <hr/>
                    <ul>
                          <Checkbox categories={categories}
                                    handleFilters= {filters=>
                                        handleFilter(filters, "category")
                                    }/>
                    </ul>

                     <hr/>

                     <h4 className="ml-3"> Price Range</h4>
                     <div className="price-slider">
                         {filteredResults &&   <InputRange
                            maxValue={maxValue}
                            minValue={minValue}
                            value={priceRange}
                            step={1}
                            onChange={(value) => changeValue(value)}
                            onChangeComplete={value => completeChange(value)} 
                        />}
                    
                        </div>
                     <hr/>
                     {/* <div>
                        <Radiobox prices={prices}
                                    handleFilters= {filters=>
                                        handleFilter(filters, "price")
                                    }/>
                     </div> */}
                   
                
                </div>
                 <div className="col-9">
                        <h2 className="mb-4">
                                    Products
                        </h2>
                        <div className="row">
                                {filteredResults && filteredResults.map((p,i)=>(
                                    
                                       <div key={i} className="col-3 mb-3">
                                             <Card  product={p} />
                                       </div>
                                    
                                ))}
                        </div>
                        <hr/>
                        {loadMoreButton()}
                </div>
            </div>

            
            

        </Layout>
    )
}

export default ShopSubCategory;