import React, {useState, useEffect} from 'react'
import Layout from './Layout'
import {getproducts} from './apiCore'
import Card from './Card'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css"; 
import { Link } from 'react-router-dom';

const Home = () => {

    const [productsBySell, setProductsBySell] = useState([]);
    const [productsByArrival, setProductsByArrival] = useState([]);
    const [error, setError] = useState(false);


    const loadProductBySell = () => {
        getproducts('sold').then(data=>{
            if(data && data.status && data.status === "error"){
                setError(data.message)
            }else{
                if(data) setProductsBySell(data.data)
            }
        })
    }


    const loadProductByArrival = () => {
        getproducts('createdAt').then(data=>{
            if(data && data.status && data.status === "error"){
                setError(data.message)
            }else{
                if(data)
                setProductsByArrival(data.data)
            }
        })
    }

    const responsive1 = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 1
        },
        desktop: {
            breakpoint: { max: 2000, min: 1024 },
            items: 1
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 1
        },
        mobile: {
            breakpoint: { max: 400, min: 0 },
            items: 1
        }
};

    useEffect(() => {
        loadProductBySell();
        loadProductByArrival();
    }, [])


    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 5
        },
        desktop: {
            breakpoint: { max: 2000, min: 1024 },
            items: 5
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 2
        }
};



    const responsive3 = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 5
        },
        desktop: {
            breakpoint: { max: 2000, min: 1024 },
            items: 5
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 5
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 5
        }
};




    
    return (
        <Layout title="Home Page" description="Shopping sathi">

  <div style={{   borderCollapse: "separate",borderSpacing: "15px" , border: "5px solid lightblue"}}>
          <Carousel
                swipeable={true}
                draggable={false}
                showDots={true}
                responsive={responsive1}
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlay={true}
                autoPlaySpeed={1000}
                keyBoardControl={true}
                customTransition="all .5"
                transitionDuration={500}
                containerClass="carousel-container"
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
                >
             
                <div><img src={require("../Resources/Image/bann.png").default} className="responsive"/></div>
                <div><img src={require("../Resources/Image/ban2.png").default} className="responsive"/></div>
               
                </Carousel>
            </div>

        {/* <div className = "jumbotron"> */}
                {/* <div className="row">
                    <div className="col-12">
                        <h1> CATEGORY</h1>
                    </div>
                </div> */}
                <div className="sellers">
                     <h2 className="mb-4 text-family">
                            Shop By Category
                    </h2>
                    <hr />
                 <Carousel responsive={responsive3}>
                     <div className="ml-2">
                        <Link to={`/category/Men's Wear`}>
                            <div>
                                <span className="cat_circle" >
                                    <img src={require('../Resources/Image/man.png').default}  height="80px" alt="Avatar"  /> <br />
                            </span>
                            
                                        <span className="ml-3">Men  </span>
                            
                                    
                            </div>
                        </Link>
                     </div>

                     <div className="ml-2">
                        <Link to={`/category/Women's Wear`}>
                            <div >
                                <span className="cat_circle">  
                                    <img src={require('../Resources/Image/woman.png').default}   alt="Avatar"  /> <br />
                                </span>
                            
                                <span className="ml-2">women  </span>
                            
                            </div>
                        </Link>
                     </div>

                     <div className="ml-2">
                        <Link to={`/category/Electrical`}>
                            <div >
                                <span className="cat_circle">  
                                    <img src={require('../Resources/Image/electrical.png').default}   alt="Avatar"  /> <br />
                                </span>
                            
                                <span className="ml-1">Electrical  </span>
                        
                            </div>
                        </Link>
                     </div>
                     <div className="ml-2">
                           <Link to={`/category/House & Furnture`}>
                                <div >
                                    <span className="cat_circle">  
                                        <img src={require('../Resources/Image/furniture1.png').default}   alt="Avatar"  /> <br />
                                    </span>
                                
                                    <span className="mr-3">House & Furniture  </span>
                            
                                </div>
                            </Link>
                     </div>
                    
                     <div className="ml-2">
                          <Link to={`/category/Kids & Toys`}>
                                <div >
                                    <span className="cat_circle">  
                                        <img src={require('../Resources/Image/kids.png').default}   alt="Avatar"  /> <br />
                                    </span>
                                
                                    <span className="mr-5">Kids and toys  </span>
                            
                                </div>
                            </Link>
                     </div>
                     
                {/* 

                

               

              

             
                  <div  >
                     <span className="cat_circle">  
                         <img src={require('../Resources/Image/sports.png').default}   alt="Avatar"  /> <br />
                    </span>
                   
                     <span className="mr-5">sports & books  </span>
               
                </div> */}
                </Carousel>
            </div>
        {/* </div> */}
           

             <div className="sellers">
                <h2 className="mb-4 text-family">
                    Best Sellers
                </h2>

                <Carousel responsive={responsive}>
                    {productsBySell && productsBySell.map((product,i) => 
                        (
                            <div key={i}>
                                    <Card  product={product} />
                            </div>
                    ))}
                    </Carousel>
            </div>
          


           <div className="sellers">

            <h2 className="mb-4 mr-7 text-family">
              New  Arrivals
            </h2>

             <Carousel responsive={responsive}>
                {productsBySell && productsBySell.map((product,i) => 
                    (
                       <div key={i} >
                                <Card  product={product} />
                        </div>
                ))}
                </Carousel>

           
</div>
            
            

        </Layout>
    )
}


export default Home;