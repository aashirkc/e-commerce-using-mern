import React, {useState, useEffect} from 'react'
import Layout from './Layout'
import {getproducts, listRelated, read} from './apiCore'
import Card from './Card'


const Product = (props) => {
    const [product, setProduct] = useState({})
    const [relatedProduct, setrelatedProduct] = useState([])
    const [error, setError] = useState(false)

    const loadingSingleProduct = productId => {
        read(productId).then(data => {
            if(data && data.status === "error"){
                setError(data.message)
            }else{

                setProduct(data.data)
                console.log(data.data._id)
                listRelated(data.data._id)
                .then(data=>{ 
                    console.log("sdfdsfs",data)
                         if(data.status && data.status === "error"){
                            setError(data.message)
                         }else{
                             console.log("sdfasdfasdf")
                            setrelatedProduct(data.data);
                         }
                    })
            }
        })
    }


    useEffect(() => {
        const productId = props.match.params.productId
        loadingSingleProduct(productId)
    }, [props])


    return(
     <Layout title={product && product.name} description={product &&  product.description}>
            <h2 className="mb-4">Single Product</h2>
            <div className="row">
              <div className="col-8"> 
                  {product &&  product.description && <Card product={product } showViewProductButton={false}/>}
              </div>

               <div className="col-4"> 
                    <h4>Related Product</h4>
                    {relatedProduct && relatedProduct.map((p,i)=>(
                        <div key={i} className="mb-3">
                            <Card  product={p} />
                        </div>
                    ))}
                </div>
            </div>
        </Layout>
    )
}

export default Product;