import React, {Fragment, useEffect} from 'react';
import { Link, withRouter } from 'react-router-dom';
import { signout , isAuthenticated} from '../Auth';
import {itemTotal} from './CartHelper'
import Search from './Search';
import {getCategorySub} from './apiCore'
import { useState } from 'react';

const isActive = (history, path) => {
    if(history.location.pathname === path){
        return {color: '#ff9900'}
    } else {
        return {
            color:'black'
        }
    }
}

const Menu = ({history}) => {

    const[categories, setCategories] = useState([]);

    useEffect(()=>{
        getCategorySub().then(data=>{
             if(data && data.status && data.status === "error"){
               console.log(data)
            }else{
                if(data)  setCategories(data.data)
            }
        })
    }, [])

return(
    <div className="sticky-top top-menu" style={{borderBottom: "1px solid #dee2e6"}}>
        <ul style={{height: "45px"}} className="nav nav-tabs bg-white">

              <li className="nav-item">
                <Link className="nav-link" style={isActive(history, '/')} to="/">Home</Link>
            </li>


              <li className="nav-item">
                <Search />
            </li>

              {/* <li className="nav-item">
                <Link className="nav-link" style={isActive(history, '/shop')} to="/shop">Shop</Link>
            </li> */}


            <li className="nav-item">
                <Link className="nav-link" style={isActive(history, '/cart')} to="/cart">
                    <i className="fa fa-shopping-cart"></i> Cart
                    <sup><small className="cart-badge">{itemTotal()}</small></sup>
                </Link>
            </li>
            
            { isAuthenticated() && isAuthenticated().data.role === 0 && (
                <li className="nav-item">
                    <Link className="nav-link" style={isActive(history, '/user/dashboard')} to="/user/dashboard"><i className="fa fa-user"></i>Profile</Link>
                </li>
            )}

            { isAuthenticated() && isAuthenticated().data.role === 1 && (
                <li className="nav-item">
                    <Link className="nav-link" style={isActive(history, '/admin/dashboard')} to="/admin/dashboard"><i className="fa fa-user"></i> Dashboard</Link>
                </li>
            )}


          

            { !isAuthenticated() && (
                <Fragment>
                    <li className="nav-item">
                        <Link className="nav-link" style={isActive(history, '/signin')} to="/signin">Signin</Link>
                     </li>

                     <li className="nav-item">
                          <Link className="nav-link" style={isActive(history, '/signup')} to="/signup">Signup</Link>
                     </li>
                </Fragment>
            )}

            { isAuthenticated() && (
                 <li className="nav-item">
                    <span className="nav-link"
                        style={{cursor: 'pointer', color:'#ff9900'}} 
                        onClick={() => signout(() => {
                                         history.push('/')
                                 })}
                                 >
                                 Sign out   
                    </span>
                
                </li>
            )}

            

           
        </ul>
        <div className="navbar">
            {categories && categories.map((c,i)=>(
                 <div key={i} className="dropdown">
                    <button className="dropbtn">{c.name}
                        <i className="fa fa-caret-down"></i>
                    </button>
                    <div className="dropdown-content">
                        {c.subCategory.map((s,i)=>(
                            
                                <Link key={i} to={`/subcategory/${s._id}`}>
                                      {s.name}
                                </Link>
                              
                        ))}
                    </div>
                    
                </div> 
            ))}
             
        </div>
        
    </div>
)
}

export default withRouter(Menu);

